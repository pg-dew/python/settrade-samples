from settrade_v2 import Investor
from settrade_v2.errors import SettradeError
import configparser

cfgParser = configparser.RawConfigParser()
cfgParser.read('../../settrade-api.cfg')

app_id = cfgParser.get("AUTH", "app-id")
app_secret = cfgParser.get("AUTH", "app-secret")
broker_id = cfgParser.get("AUTH", "broker-id")
app_code = cfgParser.get("AUTH", "app-code")


investor = Investor(
    app_id,
    app_secret,
    broker_id,
    app_code,
    is_auto_queue=False
)

market = investor.MarketData()
try:
    res = market.get_candlestick(
        symbol="BBL",
        interval="5m",
        limit=10,
        normalized=True,
        start="2022-10-01T08:00",
        end="2022-11-08T19:00"
    )
    print(res)
except SettradeError as e:
    print("---- error message  ----")
    print(e)
    print("---- error code ----")
    print(e.code)
    print("---- status code ----")
    print(e.status_code)
