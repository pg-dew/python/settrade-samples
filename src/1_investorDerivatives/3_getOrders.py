from settrade_v2 import Investor
from settrade_v2.errors import SettradeError
import configparser

cfgParser = configparser.RawConfigParser()
cfgParser.read('../../settrade-api.cfg')

app_id = cfgParser.get("AUTH", "app-id")
app_secret = cfgParser.get("AUTH", "app-secret")
broker_id = cfgParser.get("AUTH", "broker-id")
app_code = cfgParser.get("AUTH", "app-code")


investor = Investor(
    app_id,
    app_secret,
    broker_id,
    app_code,
    is_auto_queue=False
)

deri = investor.Derivatives(account_no="dewkul-D")
try:
    info = deri.get_orders()
    print(info)
except SettradeError as e:
    print("---- error message  ----")
    print(e)
    print("---- error code ----")
    print(e.code)
    print("---- status code ----")
    print(e.status_code)
