from settrade_v2 import Investor
from settrade_v2.errors import SettradeError
import configparser
import time

cfgParser = configparser.RawConfigParser()
cfgParser.read('../../settrade-api.cfg')

app_id = cfgParser.get("AUTH", "app-id")
app_secret = cfgParser.get("AUTH", "app-secret")
broker_id = cfgParser.get("AUTH", "broker-id")
app_code = cfgParser.get("AUTH", "app-code")


investor = Investor(
    app_id,
    app_secret,
    broker_id,
    app_code,
    is_auto_queue=False
)

realtime = investor.RealtimeDataConnection()


def my_message(result):
    print(result)


sub = realtime.subscribe_equity_order(
    "dewkul-E",
    on_message=my_message,
)
sub.start()

# run main thread forever
while True:
    time.sleep(1)
